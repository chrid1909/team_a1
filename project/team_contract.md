## Norms, rules and procedures for our team

- Notify the team in due time if you are delayed or absent
- The team could meet during school hours if it’s possible.
- If we are flexible teamwork will be easier
- Agreed appointments and deadlines must be met by all team members
- We have confidentiality within the team and do not slander about other team members
- Our meetings always have an agenda which is facilitated by one of the team members
- Should a minor interpersonal conflict erupt between team members they should work it out in private between themselves
- Should a major interpersonal conflict erupt between team members the team should come together and discuss, possibly with a 3rd party mediator
- We all have a responsibility that the agenda is fulfilled at meetings
- We start and end team meetings with an update/recap
- We write minutes of meetings everytime we meet
- We use the agreed sharing platform, which is discord and google docs for reacaps of meetings and shared study material
- We work seriously but also allow space for fun!!!!
- We support each other and accept each others differences
- There are no stupid questions or ideas
- Be honest!
- Work concentrated towards the goal and with high work ethic
- Don't judge yourself, you are good enough!
- Good team work is flexibility, mutual respect and recognition, structured work and a constructive attitude
- We expect every team member to be focused and do their best.
- We make sure that every meeting is documented and we take turns documententing/writing summaries/resumés.
- Make sure that everyone gets heard, and don’t interrupt each other.

