#!/usr/bin/env python
import math
import turtle

turtle.tracer(200)
bob = turtle.Turtle()
bob.hideturtle()

# recursively draw a Koch curve
#   t      is the turtle
#   s      is the length
#   depth  is the limit for the recusion
def koch(t, s, depth):
  if depth <= 0:
    # base case - draw a line
    t.fd(s)
  else:
    # step case - draw four Koch curves
    koch(t, s/3, depth-1)
    t.lt(60)
    koch(t, s/3, depth-1)
    t.rt(120)
    koch(t, s/3, depth-1)
    t.lt(60)
    koch(t, s/3, depth-1)

# position the turtle
bob.pu()
bob.bk(121)
bob.rt(90)
bob.bk(60)
bob.lt(90)
bob.pd()

# draw three Koch curves to obtain a snowflake
for i in range(3):
  koch(bob, 243, 5)
  bob.rt(120)
turtle.update()
turtle.mainloop()
