# Background

-


# Purpose

-


# Goals

-


# Project deliveries are:  

-


# Schedule

-


# Organizing of the project

Supervisors
Developers
Customers


# Budget and resources

-


# Risk assessment

1. Someone didn’t do their job

	- Weekly reports about progress could indicate problems

	- Checkups during team meetings

2. Hardware Malfunction/errors

	- Redundancy

	- Troubleshooting

3. Miscommunication

	- Team meetings to establish proper communication

	- Make sure everyone understands what is being said

4. Mismanagement of time

	- Proper planning for example microsoft project, calendars and documents.

5. Documentation

	- Make sure you do it :-)

6. Too ambitious

	- Stick to the plan

	- K.I.S.S. Keep it simple, Stupid!

7. Sickness / Other injuries

	- In case of sickness / injuries, make sure work is documented and delegated.

	- Communicate!

8. Lack of technical knowledge

	- Google, reading, ask your team and lecturers.

	- Bad Code

	- Ask your team, google and documentation

9. Didn’t ask for help

	- Don’t be shy to ask for help, no matter the problem/question.

10. Personal Complications/Disagreements

	- Should a minor interpersonal conflict erupt between team members they should work it out in private between themselves

	- Should a major interpersonal conflict erupt between team members the team should come together and discuss, possibly with a 3rd party mediator

11. Bad planning

	- Plan thoroughly

	- Have someone analyze the plan

	- Don’t be afraid to change the plan

12. Didn’t have a plan

	- Talk with lecturers about how to properly plan

13. Misunderstood the project

	- In team meetings, have focus and ask questions about the parts of the project you don’t understand.

14. No backups

	- Make backups to third party storage services such as Google Drive, Dropbox etc.

15. Lost interest of project

	- Make sure everyone is involved in planning the project, so that we make sure everyone at least finds it a bit interesting.

16. Afraid to speak up about problems (Hardware issues, etc)

	- Try and speak up, have better team relations.


# Stakeholders & Communication

- Us

	Communication: Discord / Messenger / Element / Physical 

- Potential customers

	Communication: GoFundMe / Email

- Teachers

	Communication: Element / Discord / Email / Physical 

- Our families

	Communication: Physical / SMS / Phone Calls

- Potential employers

	Communication: LinkedIn

There are no diverse interests in our stakeholders.


# Perspectives

-


# Evaluation

After a project has been completed, we will go over what we learned, what was good and was bad.


# References

-