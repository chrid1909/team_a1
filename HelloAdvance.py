#This program tests import, conversion of int to string and basic calculations

import datetime
nowTime=datetime.datetime.now()

print('Hello there')
print('What is your name little one')
myName=input()
print('It is a pleasure meeting you ' + myName)
lenName=len(myName)
print('Did you know your name is ' + str(lenName) + ' letters long?')
print('By the way how old are you?')
myAge=input()
print('Wow you are ' + str(myAge) + ' old. That means you were born in ' + str(nowTime.year-myAge))
